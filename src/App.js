import React, { Component } from "react";
import _ from "lodash";
import { get_sources } from "./services/newsapi";

import NewsItemContainer from "./components/NewsItemContainer";
import TokenValue from "./components/TokenValue";

// get our fontawesome imports
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee, faThumbsUp, faShareAltSquare, faBookmark, faEye,  faComments, faPhoneSquare, faShoppingBag, faBell, faCompress } from '@fortawesome/free-solid-svg-icons'
library.add(fab, faCheckSquare, faCoffee, faThumbsUp, faShareAltSquare,faBookmark,faEye,faComments,faPhoneSquare,faShoppingBag,faBell,faCompress)

class App extends Component {
  state = {
    sources: []
  };

  async componentDidMount() {
    const _sources = await get_sources();

    this.setState({ sources: _.sampleSize(_sources, 3) });
  }

  render() {
    return (
      <div className="container">
        <div className="card-container">
          <NewsItemContainer />
        </div>
      </div>
    );
  }
}

export default App;
