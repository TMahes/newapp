import React, { Component } from "react";
import _ from "lodash";
import * as jwt_decode from 'jwt-decode';
import { get_accesstoken } from "../services/newsapi";
import Token from "./Token";


export default class TokenValue extends Component {
  state = {
    accesstokens: []
  };

  async componentDidMount() {
    const _accesstokens = await get_accesstoken();

    this.setState({ accesstokens: jwt_decode(_accesstokens.data.status.accesstoken) });
  }

  render() {
    return (
      <div className="container">
        <div className="card-container">
            {this.state.accesstokens.result}
          
        </div>
      </div>
    );
  }
}

//export default App;
