import React, { Component } from "react";
import moment from "moment";
import * as jwt_decode from 'jwt-decode';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Modal from "react-bootstrap/Modal";
import LazyLoad from 'react-lazyload'
import { faBorderNone } from "@fortawesome/free-solid-svg-icons";

export default class NewsItem extends Component {
	render() {
		const containerStyle = {
			width: "max-width: 740px;"
		  };
//display news feeds and embed to NewsItemContainer
		return (
			 <div className="card mb-3" style={containerStyle}>
			<div className="row no-gutters">
				    <div className="col-sm-12">
							<div class="card-header" id="headingOne">
							<p>
							<button className="btn" type="button" data-toggle="collapse" data-target={"#collapse"+this.props.item.id} aria-expanded="true" aria-controls={this.props.item.id}>
									<h6 className="card-title">
										{this.props.item.title}
									</h6>
								</button>
							</p>					
							</div>
						<div id={"collapse"+this.props.item.id} className="collapse multi-collapse">
							<div className="card-body">
								<iframe src={this.props.item.url}  title={this.props.item.id} height="700px" width="100%" />
							</div>
						</div>
							<div class="card-footer text-muted">
							{ this.props.item.like &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="thumbs-up" /> Like
        						</button>
	 						}
							{ this.props.item.shared &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="share-alt-square" /> Share
        						</button>
	 						}
							{ this.props.item.bm &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="bookmark" /> Bookmark
        						</button>
	 						}
							{ this.props.item.views &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="eye" /> Views
        						</button>
	 						}
							{ this.props.item.chat &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="comments" /> Chat
        						</button>
	 						}
							{ this.props.item.call &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="phone-square" /> Call
        						</button>
	 						}
							{ this.props.item.shop &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="shopping-bag" /> Shop
        						</button>
	 						}
							{ this.props.item.alert &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="bell" /> Alert
        						</button>
	 						}
					 		{ this.props.item.compare &&
								<button type="button" class="btn btn-default btn-sm" >
          						<FontAwesomeIcon icon="compress" /> Compare
        						</button>
	 						}
  							</div>
					</div>
			</div>
		</div>
		);
	}
}
var token = '{"status":{"accesstoken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJjaGVja3N1bSI6IjA1YjY1YTE0YWIwOTI4NTMyZmM4Yzg3ZjAxNDU0NWVkIiwiZXhwIjoxNTk3MzUwNTQ3LCJyZXN1bHQiOiJVRXRxaWlrSldYOXBSNnZzczk3cWdJUm44NWtjQ1lLVnp4N2lXSklUTUprWi1vSWNaZlV3M2wzOEtGNXNhZmQ5TFZqRWpuZDAwcnlzTDJxR0FnNGNZOWJ1WDM2emFMdU1na29QWWEtMnBZak1fMjdkd0E0ZWM2TXhqblZZcmlyc003VlNYbmVEOUdsbUpuOGoxbTlNdFQ3ckhOc0g5NUxFaVlIWHBuLUxVWEduNmg5XzRRT0t2blRNLTFnQldlbDFJS0pLZEpEVGluNHJSRkYyUDRjZGJmSF9SMVJYY3NmNEk2TVVxVmdjTVh6aXRiNVdvM0hmWDU5SmZLMmY1ZkgtYkRyNW03VDhzeXlTbzFuUXdWdGJaQVdIMUVKU2x2VXN0dTFBZEgwajV1cVMzb2RaSFdSYnRqUlVpa0hzMmpBMzdMeGNCRTJtQ0huMkh6QURZZzNseHozdXNYLVcxeVdhYU1YVUFKRm1iSXFjUWxnMFZ6ZlJ4U2pGd1I1cUYwS0RuSU15dG4wbDVIeW0zTThZVTlhZVlVOEYxVmhyYTRjN05sMXVySFgyLU1saktJU1pWN0RDZ1VDb3hSM1UwUVNQNFVJd2VuYkhrNWxNc2dmMEFpN2QxVk8xZlZDU2N0UGJZVENDYnl1TWNmc3doT0RucHFFdl9pVHVjOVVBbS1WN2xHM2FSWXMtTWRrY1QzMGhwTWQwdDJKZEZXcHFibi1Bc0g2S2d6bnN1Q21wRHVqYmp1ZWgyRFFlTXVFTTM0Ty1BNlNROFVuTUprQnI2UVlvMG9pNkNjRkFpN1dQSEhSM1RtdWpzVkJqbDdjSFpqMzVQczN1YnpHSEJRNTVQSUFZb3M3YmNKOXZ1bXBzOUppYWNqNWZTS3dzNTVmdGFzUmNzd0RLeW1ndGZlR0tySWF2LUV4Q29zRndVNnMxWWtqSVVRRlNkdFVCaXM1LVA0YmVUamg4WlN6bUFkd3pvX0U3VUF6VWk5TXRtbTgyVExOSjN0YjUtQkl0MWU4RF92ZnRhMlBOZ0NuOFBxWVExUGE3bl92U3REcWp5bzU5dDNhdDh1NFVFblEtcDdjQWlxaVZVanQwSkpEU1YtMEFvVnhuQURPcThjVy1pQXREcnNram1za25ZTnlGZzhYWmh3dkVKQ3JSNnVEbGpDYW5EOEIyMjBiXzFrYkFmclRJYXBfUUFGN1BtM296eVJVb3lsTlRkRUFBSE5BYUk5T0h0d25feGNZc21iV044NzJJZ3VyRU1sQThRdGpWUEhFclhwa3VHemRVX183TWlTQmttTm84N1RsVkNPbnhEQkFwVGFIOF9BbTBYaFBBa1lhM3hrLU13THdtRG5zVXB6bFA0STdXb09hYUJrdGJTcWh1aTctdVM5UEhGRlk4QzNxNXdtQVJwYldaZkVBUl9ISnV6NGdvLTNRckVVQkFXNW43T2VSRnFKYVNBSmdzVE1wNDhJYi02NlIxbnBJPSIsInN0YXR1cyI6Im9rIn0.nKqTjNI2jHUhVW2N6xvwko1XWgpu8OL5kNqMbgM8M8w"}}';
 
var decoded = jwt_decode(token);
console.log(decoded);