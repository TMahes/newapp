import axios from "axios";
import _ from "lodash";
//Get source root from Json
const get_sources = () => {
  return axios
    .get(
      "https://newsapi.org/v2/sources?language=en&apiKey=" +
        process.env.REACT_APP_NEWS_API_KEY
    )
    .then(res => {
      return _.sampleSize(res.data.sources, 1);
    })
    .catch(err => {
      console.log(err.message);
      alert(err.message);
    });
};
//Get article from Json
const get_articles = source => {
  return axios
    .get(
      "https://newsapi.org/v2/top-headlines?sources=" +
        source +
        "&apiKey=" +
        process.env.REACT_APP_NEWS_API_KEY
    )
    .then(res => {
      return _.sampleSize(res.data.articles, 10);
    });
};


const options = {
  headers: {
    'Content-Type': 'application/json'}
};
const get_accesstoken = () => {
 return axios
    .post(
      "http://74.208.252.70:3000/api/v2/stream/list",
      {
        phone:"4084804395"
      },
      options
    )
    .then((response) => {
      console.log(response);
      return response;
    }, (error) => {
      console.log(error);
    });
  }
export { get_sources, get_articles,get_accesstoken };
